/*
 * X.h
 *
 *  Created on: 11.05.2017
 *      Author: RENT
 */

#ifndef X_H_
#define X_H_

class X {
public:
	X();
	virtual ~X();
	int getID();
	static void setCurrent_id(int _current_id);

private:
	static int current_id;
	int id=0;
};

#endif /* X_H_ */
