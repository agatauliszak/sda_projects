/*
 * Room.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef ROOM_H_
#define ROOM_H_
#include <iostream>
#include <string>
using namespace std;


class Course;

class Room {
public:
	Room ();
	Room (int id, int capacity,string building);
	Course * wsk_kursy [10];
	int ilosc_kursow;
	void description();
	void addCourse(Course * kurs);
	void printCourses();

private:
		int room_ID;
		int room_max;
		string building_ID;
};

#endif /* ROOM_H_ */
