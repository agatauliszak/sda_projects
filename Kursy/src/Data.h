/*
 * Data.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef DATA_H_
#define DATA_H_



class Data {
public:
	Data (int dzien, int miesiac, int rok);
	void printDate() const;
	void setDay (int day);
	void setMonth (int month);
	void setYear (int year);

private:
	int day;
	int month;
	int year;
};

#endif /* DATA_H_ */
