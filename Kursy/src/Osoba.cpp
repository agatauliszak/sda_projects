/*
 * Osoba.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "Osoba.h"
#include "Course.h"

Osoba::Osoba (string jakImie, string jakNazwisko, int jakiWiek): Imie(jakImie), Nazwisko(jakNazwisko), wiek(jakiWiek)
{ilosc_kursow=0;}

void Osoba::description()
{
	cout <<Imie<< " " << Nazwisko<< ", wiek "<<wiek<< " lat;  mail: "<<createMail()<< endl;
}

string Osoba::createMail()
{
	return  Imie + "."+Nazwisko+("(malpa).ku.jp");
}

void Osoba::addCourse(Course * kurs)
{ if (ilosc_kursow<10){
	wsk_kursy [ilosc_kursow]= kurs;
	ilosc_kursow ++;}
else
	cout <<"Limit wyczerpany na ten semestr, nie mozesz dodac kursow!"<<endl;
}

void Osoba::printCourses(){
	description();
	cout <<"Przypisane kursy: "<<endl;
	for (int i=0; i<ilosc_kursow ;i++)
		cout << wsk_kursy[i]->topic<< endl;
}
