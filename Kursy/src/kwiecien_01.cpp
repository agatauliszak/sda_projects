//============================================================================
// Name        : kwiecien_01.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include<string>
#include "Room.h"
#include "Osoba.h"
#include "Student.h"
#include "Master.h"
#include "Course.h"
#include "Data.h"
using namespace std;

int main() {
	Room tab_rooms [3]={ Room (3, 25,"A3"), Room (12, 22, "A2"), Room (1, 22, "B1")};
	tab_rooms[2].description();
	Student tab_student [7]= {Student ("Kalina", "Malina", 22), Student ("Ula", "Cebula", 36), Student ("Tadek", "Niejadek", 44),
			Student ("Mala", "Mi", 18), Student ("Hare", "Kriszna", 25), Student ("Lao", "Che", 33),  Student ("Jancio", "Wodnik", 88)};
	Master tab_mistrzow [10]= {Master ("Francis", "deWall", 45),
			Master  ("Tetsuro", "Matzuzawa", 46),
			Master  ("Jared", "Diamont", 80),
			Master  ("Jane", "Goodall", 83),
			Master  ("Michael", "Tomasello", 67),
			Master  ("Walter", "Burkert", 88),
			Master  ("Mary", "Douglas", 77),
			Master ("CarlGustaw", "Jung", 88),
			Master ("Emile", "Durkheim", 100),
			Master  ("Mircela", "Eliade", 111)};
Data a (2, 02, 2017);
Data b (10, 06, 2017);
Course aaa (a, b, "Antropologia religii", "Podstawowe zagadnienia zjawiska religii z perspektywy antropologii strukturalnej");
aaa.addRoom(&tab_rooms[1]);
aaa.addMaster (&tab_mistrzow[2]);
aaa.addMaster (&tab_mistrzow[3]);
aaa.addStudent(&tab_student[1]);
aaa.courseDescription();
tab_rooms[1].printCourses();
tab_mistrzow[2].printCourses();
tab_student[1].printCourses();
	return 0;
}
