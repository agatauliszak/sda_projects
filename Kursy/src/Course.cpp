/*
 * Course.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "Course.h"
#include"Room.h"
#include "Master.h"
#include "Student.h"

Course::Course( Data pocz_data, Data kon_data, string temat, string opis): startData(pocz_data), endData(kon_data), topic(temat),
					description(opis), wsk_sala(0), iloscMaster(0), iloscStud(0), iloscSal(0) {}


void Course::addMaster(Master* ktos) {
	if (iloscMaster < 3) {
		wsk_master[iloscMaster] = ktos;
		ktos->addCourse(this);
		iloscMaster++;
	} else
		cout<< "Nie mozna dodac prowadzacego! Jest ich juz wystarczajaco duzo. "<< endl;
}

void Course::addRoom(Room * sala) {
	if (iloscSal<1) {
	wsk_sala = sala;
	wsk_sala->addCourse(this);
	} else
		cout <<"Nie mozna dodac dodatkowej sali!"<< endl;

}

void Course::addStudent(Student* osoba) {
	if (iloscStud < 15) {
		wsk_student[iloscStud] = osoba;
		osoba->addCourse(this);
		iloscStud++;
	} else
		cout << "Nie mozna dodac studenta! Zapisy na kurs zakonczone. " << endl;
}

void Course::courseDescription() {
	char kreska []=
	"--------------------------------------------------------------------------------------\n";
	cout <<kreska
	<<"Nazwa kursu: "<< topic<<"\n"
	<<"Opis: "<<description<<"\n"
	<<"Data rozpoczecia: ";
	startData.printDate();
	cout <<"Data zakonczenia: ";
	endData.printDate();
	cout <<"Adres: ";
	wsk_sala->description();

	cout <<"Prowadzacy: "<<endl;
	for (int i=0; i<iloscMaster; i++){
	cout <<"Prof. ";
	wsk_master[i]->description();}

	cout <<"Uczestnicy: "<<endl;
	for (int i=0; i<iloscStud; i++){
	wsk_student[i]->description();}
	cout <<kreska;
}

