/*
 * Student.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef STUDENT_H_
#define STUDENT_H_

#include "Osoba.h"

class Student: public Osoba {
public:
	Student (string jakImie, string jakNazwisko, int jakiWiek);

};

#endif /* STUDENT_H_ */
