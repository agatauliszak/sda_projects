/*
 * Data.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "Data.h"
#include<iostream>
using namespace std;

Data::Data(int dzien, int miesiac, int rok): day(dzien), month (miesiac), year(rok) {}

void Data::printDate() const
{
	cout << year << "-";
	if (month < 10) {
		cout << 0;	//dopisanie 0
	}
	cout << month << "-";
	if (day < 10) {
		cout << 0;
	}
	cout << day << endl;
}


void Data::setDay(int nday) {
	if (nday<31 && nday>>0){
	day = nday;}
	else
		cout<<" Data niepoprawna"<<endl;
}

void Data::setMonth(int nmonth) {
	month = nmonth;
}
void Data::setYear(int nyear) {
	year = nyear;
}
