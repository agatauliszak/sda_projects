/*
 * Course.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef COURSE_H_
#define COURSE_H_
#include "Osoba.h"
#include "Data.h"
#include <iostream>
#include <string>
using namespace std;

class Room;
class Master;
class Student;

class Course {

public:
	Course ();
	Course(Data pocz_data, Data kon_data, string temat, string opis);
	Data startData;
	Data endData;
	string topic;
	string description;

	Room *wsk_sala;
	Master *wsk_master[3];
	Student *wsk_student[25];

	void addMaster (Master* ktos);
	void addRoom (Room * sala);
	void addStudent (Student* osoba);
	void courseDescription();
private:
	int iloscMaster;
	int iloscStud;
	int iloscSal;

};

#endif /* COURSE_H_ */
