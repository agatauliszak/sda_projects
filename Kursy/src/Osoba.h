/*
 * Osoba.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef OSOBA_H_
#define OSOBA_H_

#include <iostream>
#include<string>
using namespace std;

class Course;

class Osoba {
public:
	Osoba (string jakImie, string jakNazwisko, int jakiWiek);
	void addCourse(Course * kurs);
	void printCourses();
	void description();
	string createMail();
	Course* wsk_kursy [10];
	int ilosc_kursow;

protected:
	string Imie;
	string Nazwisko;
	int wiek;


};

#endif /* OSOBA_H_ */
