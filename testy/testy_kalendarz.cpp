/*
 * testy_kalendarz.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include  "gtest/gtest.h"
#include  "functions.h"

TEST(Testingkalendarz, Zero) {
  EXPECT_EQ(0, kalendarz(0,0,0));
}

TEST(Testingkalendarz, Possitive1) {
  EXPECT_EQ(1, kalendarz(1,1,1));
  EXPECT_EQ(1, kalendarz(31,1, 2012));
  EXPECT_EQ(1, kalendarz(1,12, 96));
}

TEST(Testingkalendarz, Possitive2) {
  EXPECT_EQ(0, kalendarz(42,12, 2012));
  EXPECT_EQ(0, kalendarz(13,13, 1999));
  EXPECT_EQ(0, kalendarz(31,04, 1999));
  EXPECT_EQ(0, kalendarz(32,1, 1999));
  EXPECT_EQ(1, kalendarz(31,5, 22000));
}

TEST(Testingkalendarz, LeapYear) {
  EXPECT_EQ(1, kalendarz(29,2,2000));
  EXPECT_EQ(1, kalendarz(28,2, 2013));
  EXPECT_EQ(0, kalendarz(29,2, 2013));
  EXPECT_EQ(0, kalendarz(30,2,2000));

}

TEST(Testingkalendarz, Negative) {
  EXPECT_EQ(0, kalendarz(-2,12, 2012));
  EXPECT_EQ(0, kalendarz(13,-3, 1999));
  EXPECT_EQ(0, kalendarz(18,9, -2008));
}



