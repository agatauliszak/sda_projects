/*
 * functions.cpp
 *
 *  Created on: 21.04.2017
 *      Author: RENT
 */
#include <iostream>
#include "functions.h"
#include <string>
using namespace std;

int secondPower(int number) {
	return number * number;
}

float powers(float number, float power) {
	float result = 1;

	if (power > 0)
		for (int i = 0; i < power; i++) {
			result = result * number;
		}

	else if (power < 0 && number != 0)
		number = 1 / number;
	power = (-1) * power;
	for (int i = 0; i < power; i++) {
		result = result * number;
	}

	return result;
}

long factorial(int number) {
	int i = 0;
	long result = 1;
	if (number == 0) {
		return 1;
	} else if (number>0){
		for (i = number; i > 0; i--) {
			result = i * result;
		}
		return result;}
	else
		return 0;
}

int fib(int n) {

	if (n == 0)
		return 0;
	else if (n == 1)
		return 1;
	else if (n >= 2) {
		int c = fib(n - 1) + fib(n - 2);
		return c;
	} else
		return -1; //b�ad
}

int nwd(int k, int n) {
	if (k == 0) {
		if (n > 0)
			return n;
		else
			return (-1) * n;
	}
	int c = nwd(n % k, k);
	return c;
}

bool kalendarz(int dz, int m, int rok) {
	if (rok > 0 && m > 0 && dz > 0) {
		if ((rok % 4 == 0 && rok % 100 != 0) || rok % 400 == 0) // rok przestepny
				{
			if (((m == 1 || m == 3 || m == 5 || m == 7 || m == 9 || m == 11)
					&& dz <= 31)
					|| ((m == 4 || m == 6 || m == 8 || m == 10 || m == 12)
							&& dz <= 30) || (m == 2 && dz <= 29)) {
				return 1;
			} else
				return 0;
		} else if (((m == 1 || m == 3 || m == 5 || m == 7 || m == 9 || m == 11)
				&& dz <= 31)
				|| ((m == 4 || m == 6 || m == 8 || m == 10 || m == 12)
						&& dz <= 30) || (m == 2 && dz <= 28)) {
			return 1;
		} else
			return 0;
	} else
		return 0;
}

int daysInMonth (int month)
{
	switch (month) {
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
		return 31;
	case 4:
	case 6:
	case 9:
	case 11:
		return 30;
	case 2:
		return 28;
	default:
		return 0;}
}



string cyfry (int number)
{string liczba="";
   int opposite_number=0;
   int cyfra = 0;
   int opposite_cyfra=0;

   if (number <0) {
       number = number * (-1);
   }

   while (number >0) {
       cyfra = number%10;
       number = number/10;
       opposite_number = (opposite_number*10)+cyfra;
  }

   while (opposite_number>0) {
      opposite_cyfra = opposite_number%10;
      opposite_number = opposite_number/10;

   switch (opposite_cyfra){
case 1:
   string liczba=liczba + "jeden ";
   break;
case 2:
   string liczba=liczba+ "dwa";
   break;
case 3:
	string liczba=liczba+ "trzy";
   break;
case 4:
	string liczba=liczba+ "cztery";
   break;
case 5:
	string liczba=liczba+ "piec";
   break;
case 6:
	string liczba=liczba+ "szesc";
   break;
case 7:
	string liczba=liczba+ "siedem";
   break;
case 8:
	string liczba=liczba+ "osiem";
   break;
case 9:
	string liczba=liczba+ "dziewiec";
   break;
   }
   }
   return liczba;
}
