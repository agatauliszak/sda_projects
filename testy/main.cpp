/*
 * main.cpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */

#include  "gtest/gtest.h"

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

