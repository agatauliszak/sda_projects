/*
 * test_nwd.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include  "gtest/gtest.h"
#include  "functions.h"

TEST(TestingNWD, Zero) {
  EXPECT_EQ(0, nwd(0,0));
  EXPECT_EQ(1, nwd(0,1));
  EXPECT_EQ(10, nwd(0,10));
  EXPECT_EQ(10, nwd(0,-10));
}

TEST(TestingNWD, One) {
  EXPECT_EQ(1, nwd(1,10));
  EXPECT_EQ(1, nwd(10,1));
  EXPECT_EQ(1, nwd(1,1));
}

TEST(TestingNWD, Possitive) {
  EXPECT_EQ(14, nwd(42,56));
  EXPECT_EQ(6, nwd(18,84));
}

TEST(TestingNWD, Negative) {
  EXPECT_EQ(5, nwd(-5,10));
  EXPECT_EQ(5, nwd(5,-10));
}


