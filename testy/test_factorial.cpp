/*
 * test_factorial.cpp
 *
 *  Created on: 21.04.2017
 *      Author: RENT
 */
#include  "gtest/gtest.h"
#include  "functions.h"

TEST(TestingFactorial, Zero) {
  EXPECT_EQ(1, factorial(0));
}

TEST(TestingFactorial, Possitive) {
  EXPECT_EQ(1, factorial(1));
  EXPECT_EQ(2, factorial(2));
  EXPECT_EQ(6, factorial(3));
  EXPECT_EQ(362880, factorial(9));
}

TEST(TestingFactorial, Negative) {
  EXPECT_EQ(0, factorial(-1));
  EXPECT_EQ(0, factorial(-9));
}
