/*
 * test_fib.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include  "gtest/gtest.h"
#include  "functions.h"

TEST(TestingFib, Zero) {
  EXPECT_EQ(0, fib(0));
}

TEST(TestingFib, One) {
  EXPECT_EQ(1, fib(1));
}

TEST(TestingFib, Possitive) {
  EXPECT_EQ(1, fib(2));
  EXPECT_EQ(144, fib(12));
}

TEST(TestingFib, Negative) {
  EXPECT_EQ(-1, fib(-5));
}

//TEST(TestingFib, LargeNumber) {
//  EXPECT_EQ(218922995834555169026, fib(100));
//}

