/*
 * test_dayInMonth.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include  "gtest/gtest.h"
#include  "functions.h"

TEST(TestingDays, Zero) {
  EXPECT_EQ(0, daysInMonth(0));
}

TEST(TestingDays, Two) {
  EXPECT_EQ(28, daysInMonth(2));
}

TEST(TestingDays,Numbers) {
  EXPECT_EQ(31, daysInMonth(1));
  EXPECT_EQ(30, daysInMonth(9));
  EXPECT_EQ(0, daysInMonth(21));
  EXPECT_EQ(30, daysInMonth(4));
  EXPECT_EQ(31, daysInMonth(12));
  EXPECT_EQ(0, daysInMonth(22));
}

TEST(TestingDays, Negative) {
  EXPECT_EQ(0, daysInMonth(-5));
}

