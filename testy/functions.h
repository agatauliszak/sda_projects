/*
 * functions.h
 *
 *  Created on: 21.04.2017
 *      Author: RENT
 */

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

int secondPower (int number);
float powers(float number, float power);
long factorial (int number);
int fib (int n);
int nwd (int k, int n);
bool kalendarz(int dz, int m, int rok);
int daysInMonth (int month);


#endif /* FUNCTIONS_H_ */
