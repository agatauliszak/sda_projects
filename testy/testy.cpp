/*
 * testy.cpp
 *
 *  Created on: 21.04.2017
 *      Author: RENT
 */
#include  "gtest/gtest.h"
#include  "functions.h"

TEST (TestingPowers2, One)
{
	EXPECT_EQ(1,secondPower(1));
}

TEST (TestingPowers2, Zero)
{
	EXPECT_EQ(0,secondPower(0));
}

TEST (TestingPowers2, NegativeNumber)
{
	EXPECT_EQ(25,secondPower(-5));
}

TEST (TestingPower2, LargeNumber)
{
	EXPECT_NE(1524155677489,secondPower(1234567));
}

TEST (TestingPowers, One)
{
	EXPECT_EQ(1,powers(1,1));
}

TEST (TestingPowers, Zero1)
{
	EXPECT_EQ(0,powers(0,2));
	EXPECT_EQ(0,powers(0,100));
	EXPECT_EQ(0,powers(0,-10));
}

TEST (TestingPowers, Zero2)
{
	EXPECT_EQ(1,powers(5,0));
	EXPECT_EQ(1,powers(10,0));
	EXPECT_EQ(1,powers(-10,0));
}

TEST (TestingPowers, NegativeNumber1)
{
	EXPECT_EQ(-125,powers(-5, 3));
	EXPECT_EQ(-100000,powers(-10, 5));
}

TEST (TestingPowers, NegativeNumber2)
{
	EXPECT_EQ(1,powers(1, -1));
	EXPECT_EQ(0.25,powers(2, -2));
	EXPECT_FLOAT_EQ(1e-005,powers(10, -5));
}

TEST (TestingPowers, NegativeNumber3)
{
	EXPECT_EQ(-1,powers(-1, -1));
	EXPECT_EQ(0.25,powers(-2, -2));
	EXPECT_FLOAT_EQ(-0.001,powers(-10, -3));
}
TEST (TestingPowers, LargeNumber)
{
	EXPECT_NE(1879080904,powers(1234, 3));
	EXPECT_NE(984770902183611232881,powers(3, 44));
}



