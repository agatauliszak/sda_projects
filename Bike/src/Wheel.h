/*
 * Wheel.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef WHEEL_H_
#define WHEEL_H_

class Wheel {
public:
	enum tireType {summer, winter};
	enum wheelSize {c20, c24, c26, c27, c28, c29};
	Wheel (tireType wType= summer, wheelSize s=c27);
	int getWheelSize ();
	tireType getType ();

private:
	tireType type;
	wheelSize size;

};

#endif /* WHEEL_H_ */
