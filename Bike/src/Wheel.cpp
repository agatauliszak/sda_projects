/*
 * Wheel.cpp
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#include "Wheel.h"
#include <iostream>
using std::cout;
using std:: endl;

Wheel::Wheel(Wheel:: tireType wType, wheelSize s): type(wType), size(s) {
}

Wheel::tireType Wheel:: getType () {
	return type;
}
int Wheel::getWheelSize () {
	return size;
}
