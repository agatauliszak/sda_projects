//============================================================================
// Name        : Bike.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Bike.h"
using namespace std;

int main() {
	Bike myBike;
	myBike.printBike();
	return 0;
}
Bike::Bike (){}
Bike::Bike( Wheel::tireType t, Wheel::wheelSize s, Frame::frameType f, int st): frontWheel(t,s), rearWheel (t, s), bikeFrame(f), frontLight(s),
		rearLight (s){
}

int Bike::frontWheelSize(){
	return frontWheel.getWheelSize();
}
int Bike::rearWheelSize(){
	return rearWheel.getWheelSize();
}
bool Bike::frontLightIsOn(){
	return frontLight.LightIsOn();
}
bool Bike::rearLightIsOn(){
	return rearLight.LightIsOn();
}

void Bike::printBike() {
	cout<<"Frame: "<<bikeFrame.getType()<<endl;
	cout<<"Front Wheel: "<<endl;
	cout<<"\t Wheel size: "<<frontWheelSize()<<endl;
	cout<<"\t Tire type:  "<<frontWheel.getType()<<endl;
	cout<<"Rear Wheel: "<<endl;
	cout<<"\t Wheel size: "<<rearWheelSize()<<endl;
	cout<<"\t Tire type:  "<<rearWheel.getType()<<endl;
	cout<<"Lights: "<<endl;
	cout<<"\t Front light: ";
	frontLight.LightIsOn();
	cout<<"\t Back light: ";
	rearLight.LightIsOn();
}
