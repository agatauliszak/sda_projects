/*
 * Frame.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef FRAME_H_
#define FRAME_H_

class Frame {
public:
	enum frameType{hardtail, other};
	Frame(frameType fType = other); //to jest konstruktor, zmienna nazywa sie fType ma typ frameType

	frameType getType() const {
		return type;
	}

	void setType(frameType _type) {
		type = _type;
	}

private:
	frameType type;
};

#endif /* FRAME_H_ */
