/*
 * Lighting.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef LIGHT_H_
#define LIGHT_H_

class Light {
public:
	Light(int stLight=0);
	void turnOnLight();
	void turnOffLight();
	bool LightIsOn ();
private:
int stateLight;

};

#endif /* LIGHT_H_ */
