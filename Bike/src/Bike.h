/*
 * Bike.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef BIKE_H_
#define BIKE_H_
#include "Frame.h"
#include "Wheel.h"
#include "Light.h"
#include "DriveSystem.h"

class Bike {
public:
	Bike();
	Bike(Wheel::tireType, Wheel::wheelSize, Frame::frameType,int);
	int frontWheelSize();
	int rearWheelSize();

	bool frontLightIsOn();
	bool rearLightIsOn();

	void frontTurnOnLight();
	void frontTurnOffLight();
	void rearTurnOnLight();
	void rearTurnOffLight();

	void setFrame (Frame _frame){
		bikeFrame=_frame;
	}
	Frame getFrame (){
		return bikeFrame;
	}

	Wheel getFrontWheel (){
			return frontWheel;
		}

	Wheel getRearWheel (){
			return rearWheel;
		}
	Light getFrontLight (){
			return frontLight;
		}
	Light getRearLight (){
			return rearLight;
		}
void printBike();

private:
	Frame bikeFrame;
	Wheel frontWheel;
	Wheel rearWheel;
	Light frontLight;
	Light rearLight;
	DriveSystem driveSystem;

};

#endif /* BIKE_H_ */

//rower posiada:
//kola x2								klasa
//	typ opony
//	rozmiar
//rama 								klasa
//	sztywna, amortyzowana
//hamulec x2
//amortyzator
//	rodzaj
//kierownica
//siodelko
//oswietlenie (przod, tyl)			klasa
//	metody:
//		wlacz
//		wylacz
//czy wlaczone
//uklad napedowy:						klasa
//	lancuch
//	biegi przod
//	biegi tyl
//	przerzutka
//	pedala
//  metody:
//  	  zmien bieg
//
//
//
//metody:
//	jedz
//	hamuj
//	wlacz swiatla
//	skrec (kierunek)
//	zmien bieg - jedna metoda z dwoma argumentami (przerzutka gora/dol, kierunek g�ra/d�)
//					albo 4 metody bez argumentow

