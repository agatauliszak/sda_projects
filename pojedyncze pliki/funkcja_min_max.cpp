//============================================================================
// Name        : min_max.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int minimum (int a, int b) {
	if (a<b)
	{
		return a;
	}
	return b;
}

int maximum (int a, int b) {
	if (a>b)
	{
		return a;
	}
	return b;
}

int main() {
	char wybor;
	int a;
	int min = 20000;
	int max = - 20000;
	cout << "Witaj w programie zwracajacym minimalna oraz maksymalna liczbe."
			<< endl;

	while (wybor != 't') {
		cout <<"Podaj liczb� ca�kowita: "<< endl;
				cin >>a;
		min = minimum (a, min);
		max= maximum (a, max);
		cout << "Czy chcesz zakonczyc wpisywanie? Jesli tak nacisnij 't'! "
				<< endl;
		cin >> wybor;
	}

	cout << "Min wynosi: " << min << " max wynosi: " << max << endl;

	return 0;
}
