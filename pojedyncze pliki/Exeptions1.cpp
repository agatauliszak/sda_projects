//============================================================================
// Name        : Exeptions1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

void last(){
	cout<<"Last start"<<endl;
	cout <<"Throwing int exeption"<<endl;
	throw -1;

	cout<<"Last end"<<endl;
}

void third(){
	cout<<"Third start"<<endl;
		last();
	cout<<"Third end"<<endl;
}

void second(){
	cout<<"Second start"<<endl;
	try{
		third();
	}
	catch(double){
		cerr<<"in second double exeption"<<endl;
	}
	cout<<"Second end"<<endl;
}

void first (){
	cout<<"First start"<<endl;
	try{
		second();
	}
	catch(int){
		cout<<"in first int exeption"<<endl;
	}
	catch(double){
		cout<<"in first double exeption"<<endl;
	}
	cout<<"First end"<<endl;
}
//int main() {
//	cout << "main start" << endl;
//	try{
//		first();
//
//	}
//	catch(int){
//		cout <<"main exeption catched"<<endl;
//	}
//	cout << "main end" << endl;
//	return 0;
//}
int main () {
	cout << "main start" << endl;
	try{
		int x=2;
		int y=0;
		double c=2.3;

		if (y==0)
			throw " error division by 0";
		int d=x/y;
	}
	catch (const char* exeption){
		cout <<"in main any exeption catched"<<exeption<<endl;
	}

cout << "main end" << endl;
	return 0;
}
