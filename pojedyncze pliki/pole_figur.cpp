//============================================================================
// Name        : pole_figur.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>
using namespace std;

int main() {
	int liczba = 1;
	int a = 0;
	int b = 0;
	int h = 0;
	double p, v;

	cout << "Witaj w programie obliczajacym pola i objetosci figur. " << endl;

	while (liczba > 0 && liczba < 5) {

	cout << "Wybierz figure z menu: " << endl;
	cout << "1 dla szescianu" << endl;
	cout << "2 dla prostopadloscianu" << endl;
	cout << "3 dla kuli" << endl;
	cout << "4 dla walca" << endl;
	cout << "5 by zakonczyc" << endl;

		cin >> liczba;

		switch (liczba) {
		case 1:
			cout << "Podaj d�ugosc krawedzi  szescianu: " << endl;
			cin >> a;

			p = 6 * pow(a, 2);
			v = pow(a, 3);

			cout << "Pole powierzchni prostopadloscianu wynosi:  " << p
					<< " a objetosc " << v << "." << endl<< endl;
			break;

		case 2:
			cout << "Podaj d�ugosc podstawy prostopadloscianu: " << endl;
			cin >> a;

			cout << "Podaj szerokosc podstawy prostopadloscianu: " << endl;
			cin >> b;

			cout << "Podaj  wysokosc prostopadloscianu: " << endl;
			cin >> h;

			p = (2 * a * b) + (2 * b * h) + (2 * a * h);
			v = a * b * h;

			cout << "Pole powierzchni prostopadloscianu wynosi:  " << p
					<< " a objetosc " << v << "." << endl<<endl;
			break;

		case 3:

			cout << "Podaj promien kuli: " << endl;
			cin >> a;

			p = 4 * M_PI * pow(a, 2);
			v = (0.75) * M_PI * (pow(a, 3));

			cout << "Pole kuli wynosi:  " << p << " a objetosc " << v << "."
					<< endl<< endl;
			break;

		case 4:
			cout << "Podaj promien podstawy walca: " << endl;
			cin >> a;

			cout << "Podaj  wysokosc walca: " << endl;
			cin >> h;

			p= (2 * M_PI * pow(a, 2))+(2*M_PI*a*h);
			v=M_PI * pow(a, 2)*h;
			cout << "Pole walca wynosi:  " << p << " a objetosc " << v << endl;
			cout << endl;
			break;
		}
	}
	cout << "Koniec programu!" << endl;

	return 0;
}
