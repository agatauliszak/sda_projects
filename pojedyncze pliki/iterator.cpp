//============================================================================
// Name        : iterator.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <iostream>     // std::cout
#include <iterator>     // std::ostream_iterator
#include <vector>       // std::vector
#include <algorithm>    // std::copy
using namespace std;

int main () {
	vector<int> myvector;
	vector<int>::iterator it;
  for (int i=1; i<11; ++i) myvector.push_back(i*10);

  cout<<"Myvector contains: ";
  for (it=myvector.begin(); it!=myvector.end(); it++)
	  cout <<*it<<" ";
cout<<endl;
  for (it=myvector.begin()+7; it!=myvector.end(); it++)
	  *it=(*it)*(*it);

  cout<<"After modification myvector contains: ";
  for (it=myvector.begin(); it!=myvector.end(); it++)
  	  cout <<*it<<" ";
  cout<<endl;
  cout<<"Myvector revers: ";
  for (it=myvector.end()-1; it!=myvector.begin()-1; it--)
  	  cout <<*it<<" ";
  cout<<endl;
  vector<int>anothervector(10);

  copy(myvector.begin(), myvector.end(),anothervector.begin());
cout<<"Anothervector contains: ";
  for (it=anothervector.begin(); it!=anothervector.end(); it++)
  	  cout <<*it<<" ";
  return 0;
}
