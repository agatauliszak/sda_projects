//============================================================================
// Name        : lista.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
using namespace std;

bool podzielnePrzez6(const int& value){
		return value%2==0 && value%3==0;
}

int main() {
	// constructors used in the same order as described above:
//	list<int> first;                                // empty list of ints
//	list<int> second(4, 100);                       // four ints with value 100
//	list<int> third(second.begin(), second.end());  // iterating through second
//	list<int> fourth(third);                       // a copy of third
//
//	// the iterator constructor can also be used to construct from arrays:
//	int myints[] = { 16, 2, 77, 29 };
//	list<int> fifth(myints, myints + sizeof(myints) / sizeof(int));
//
//	cout << "The contents of fifth are: ";
//	for (list<int>::iterator it = fifth.begin(); it != fifth.end(); it++)
//		cout << *it << ' ';
//
//	cout << '\n';
//
//	fifth.push_front(100);
//	fifth.push_back(101);
//
//	cout << "After changes contents are: ";
//	for (list<int>::iterator it = fifth.begin(); it != fifth.end(); it++)
//		cout << *it << ' ';
//	cout << endl;
//
//	fifth.pop_front();
//	fifth.pop_back();
//
//	cout << "After another changes contents are: ";
//	for (list<int>::iterator it = fifth.begin(); it != fifth.end(); it++)
//		cout << *it << ' ';
//	cout << endl;
//
//	cout << fifth.front();
//	fifth.begin();
//	cout << endl;
//
//	list<int> sixth(8, 200);
//	for (list<int>::iterator it = sixth.begin(); it != sixth.end(); it++)
//		cout << *it << ' ';
//	cout << endl;
//
//	sixth.resize(5);
//	for (list<int>::iterator it = sixth.begin(); it != sixth.end(); it++)
//		cout << *it << ' ';
//	cout << endl;
//	sixth.resize(10);
//	for (list<int>::iterator it = sixth.begin(); it != sixth.end(); it++)
//		cout << *it << ' ';
//	cout << endl;
//	list<int>::iterator it = sixth.begin();
//	for (int i = 0; i < 4; i++)
//		++it;
//
//	sixth.insert(it, 10);
//	for (list<int>::iterator it = sixth.begin(); it != sixth.end(); it++)
//		cout << *it << ' ';
//	cout << endl;
//	sixth.insert(it, 2, 20);
//	for (list<int>::iterator it = sixth.begin(); it != sixth.end(); it++)
//		cout << *it << ' ';
//	cout << endl;

	list<int> myList;
	list<int>::iterator it1, it2;

	for (int i=0; i<10; i++) myList.push_back(i);

	for (it1=myList.begin(); it1!=myList.end(); it1++)
		cout<<*it1<<' ';
	cout<<endl;

	it1=it2=myList.begin();
	advance (it2,9);
	++it1;
	it1=myList.erase(it1);
	it2=myList.erase(it2);

	cout<<*it1<<'\n';
	for (it1=myList.begin(); it1!=myList.end(); it1++)
		cout<<*it1<<' ';
cout<<endl;
	cout<<*it2;
	cout<<endl;

	int myint []={6, 4, 8, 10, 4, 4, 7};
	list<int> myList1 (myint, myint + 7);
	list<int>::iterator it;
	cout<<"Lista: ";
	for (it=myList1.begin(); it!=myList1.end(); it++)
			cout<<*it<<' ';
cout<<endl;

//cout<<"unique bez sortowania: "<<endl;
//myList1.unique();
//for (it=myList1.begin(); it!=myList1.end(); it++)
//		cout<<*it<<' ';
cout<<"unique po sortowaniu: "<<endl;
myList1.sort();
myList1.unique();
for (it=myList1.begin(); it!=myList1.end(); it++)
		cout<<*it<<' ';
myList1.push_back(12);
myList1.push_back(22);
cout<<endl;
cout<<"a teraz wyglada tak "<<endl;
for (it=myList1.begin(); it!=myList1.end(); it++)
		cout<<*it<<' ';
cout<<endl;
myList1.remove_if(podzielnePrzez6);
cout<<"Po zastosowaniu remove_if podzielna przez 6 lista nr 1 wyglada tak: "<<' ';
for (it=myList1.begin(); it!=myList1.end(); it++)
		cout<<*it<<' ';
cout<<endl;
myList1.push_back(3);
myList1.push_back(33);
myList1.push_back(22);
myList1.push_back(85);
myList1.push_back(1);
myList1.push_back(0);

cout<<"Po dodaniiu kilku elementow lista nr 1 wyglada tak: "<<' ';
for (it=myList1.begin(); it!=myList1.end(); it++)
		cout<<*it<<' ';
cout<<endl;

list<int> myList2;
for (int i=0; i<12; i++) myList2.push_back (i*10);
cout<<"Lista nr 2: "<<' ';
		for (it=myList2.begin(); it!=myList2.end(); it++)
		cout<<*it<<' ';
		cout<<endl;
//myList1.merge(myList2);
//cout<<"Po mergu: "<<' ';
//for (it=myList1.begin(); it!=myList1.end(); it++)
//		cout<<*it<<' ';
//cout<<endl;
it = myList1.begin();
advance(it,3);

it1=myList2.begin();
advance(it1,3);
it2=myList2.end();
advance(it2,3);

myList1.splice(it, myList2, it1, it2);

cout<<"Lista nr 1: "<<' ';
		for (it=myList1.begin(); it!=myList1.end(); it++)
		cout<<*it<<' ';
		cout<<endl;

	return 0;
}
