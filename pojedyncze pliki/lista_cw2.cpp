//============================================================================
// Name        : lista_cw2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
using namespace std;

bool isOdd (const int& value)
{
	return value%2==1;
}

bool isEven (const int& value)
{
	return value%2==0;
}

int main() {
	int myint[] = { 6, 13, 8, 10, 4, 1, 7, 11, 0, 2, 3, 9 };
	list<int> myList(myint, myint + 12);
	list<int>::iterator it;

	cout << "Lista nr 1: " << ' ';
	for (it = myList.begin(); it != myList.end(); it++)
		cout << *it << ' ';
	cout << endl;


	return 0;
}
