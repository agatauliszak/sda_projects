//============================================================================
// Name        : lista_cw1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include<list>
using namespace std;

int main() {
	int myint[] = { 6, 4, 8, 10, 4, 4, 7, 11, 0, 2, 3, 9 };
	list<int> myList1(myint, myint + 12);
	list<int>::iterator it;

	cout << "Lista nr 1: " << ' ';
	for (it = myList1.begin(); it != myList1.end(); it++)
		cout << *it << ' ';
	cout << endl;

	list<int> myList2;
	list<int>::iterator it1, it2;

	for (int i = 0; i < 12; i++)
		myList2.push_back(i * 10);
	cout << "Lista nr 2: " << ' ';
	for (it = myList2.begin(); it != myList2.end(); it++)
		cout << *it << ' ';
	cout << endl;

	it = myList1.begin();
	advance(it,3);

	it1=myList2.begin();
	advance(it1,3);
	it2=myList2.begin();
	advance(it2,8);

	myList1.splice(it, myList2, it1, it2);

	cout<<"Lista nr 1: "<<' ';
			for (it=myList1.begin(); it!=myList1.end(); it++)
			cout<<*it<<' ';
			cout<<endl;

	return 0;
}
