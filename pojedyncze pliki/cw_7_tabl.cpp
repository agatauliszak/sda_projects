//============================================================================
// Name        : cw_7_tabl.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

//Program wypisujący liczby parzyste z tablicy

#include <iostream>
using namespace std;

int main() {
	int t[10] = { 0, 5, 3, 7, 1, 43, 18, 2, 10, 43 };

	for (int i = 0; i < 10; i++) {
		if (t[i] != 0) {
			if (t[i] % 2 == 0) {
				cout << t[i] << " ";
			}
		}

	}
	return 0;
}
