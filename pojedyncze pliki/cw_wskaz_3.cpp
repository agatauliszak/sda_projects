//============================================================================
// Name        : cw_wskaz_3.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

//Program wypisujący liczby parzyste z tablicy

#include <iostream>
using namespace std;

int main() {
	int t[10] = { 0, 5, 3, 7, 1, 43, 18, 2, 10, 43 };

	int * it = t;

	for (int i = 0; i < 10; i++) {
			if ((*it) != 0) {
				if (*it % 2 == 0) {
					cout << *it <<" ";
				}
			}
			++it;
		}

	return 0;
}
