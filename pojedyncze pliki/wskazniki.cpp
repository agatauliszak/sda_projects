//============================================================================
// Name        : wskazniki.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	int a  = 13;
	int b = 52;
	int &ri = a; //referencja
	int *p = &a; // int * to typ wskaznika o nazwie p, wskazuje na adres zmiennej a

	a=17;
	ri=26;

	cout << a << " "<<&a << endl;
	cout << b<< " "<<&b << endl;
	cout <<*p <<" "<<p<< endl;
	cout <<ri <<" "<<&ri<< endl;

	return 0;
}
