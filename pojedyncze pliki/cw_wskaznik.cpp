//============================================================================
// Name        : cw_wskaznik.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	int max_array_size = 5;
	int t [max_array_size]= {1, 3, 5, 7, 9};
	int * it = t; //wskaznik na pierwszy element; dla innych juz adres trzeba pobrac
	for (int i=0; i < max_array_size; ++i){
		cout << *it++<<" "; // zmieniamy wskaznik
	}
	cout << endl;

	int *it2=t;
	for (int i=0; i<max_array_size; ++i) {
		cout << (*it2)++ <<" "; //tu zmieniamy tylko pierwszy element - zapis nawias
	}
	cout <<" "<< *it2 <<endl;

	int *t0=t; //dzialania na wskaznikach
	int *t4=&t[5]; //pokazuje ostatni element; apresant tu jest operatorem pobrania adresu
	//cout << *t<< endl;
	//cout << *t4<< endl;

	//cout <<t4-t0<< endl;
	return 0;
}
