//============================================================================
// Name        : static.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

class Foo{
public:
	Foo();
	virtual ~Foo();
	Foo (const Foo &);

	static int getV() {
		return v;
	};

	void setV(int v_) {
			v = v_;
	}
private:
	static int v; //zastosowanie : zliczenie obiektow
};
Foo::Foo ( const Foo &){
	cout <<"Tu konstruktor kopiujacy"<<endl;
	v++;
}

Foo::Foo(){
	if (v>0 && v<5){
	cout <<"Istnieje "<<++v<<" obiekty klasy Foo"<<endl;}

	else if (v==0){
	cout <<"Istnieje "<<++v<<" obiekt klasy Foo"<<endl;}
	else {
	cout <<"Istnieje "<<++v<<" obiekt�w klasy Foo"<<endl;}
}

Foo:: ~Foo(){
	if (v>2 && v<6){
		cout <<"Istnieje "<<--v<<" obiekty klasy Foo"<<endl;}

		else if (v==2){
		cout <<"Istnieje "<<--v<<" obiekt klasy Foo"<<endl;}
		else {
		cout <<"Istnieje "<<--v<<" obiekt�w klasy Foo"<<endl;}
}
 int Foo::v=0;

int main() {
	cout <<"V wynosi: "<<Foo::getV()<<endl;
	Foo x;
	Foo y;
	Foo a;
	Foo b;
	//Foo a=c;
//	cout<<x.getV()<<endl;
//	cout<<y.getV()<<endl;
//	cout<<a.getV()<<endl;
//	cout<<x.getV()<<endl;
//	cout<<y.getV()<<endl;
//	cout<<a.getV()<<endl;

	return 0;
}
