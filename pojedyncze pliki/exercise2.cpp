//============================================================================
// Name        : exercise2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int solution (int A, int B){
	if (A >=0 && A<=100000000 && B>=0 && B<=100000000){
		unsigned long long int C=A;
		unsigned  long long int product=C*B;

		int tab[64];
		int i=0;
		int sum=0;

		while (product)
		{
			tab[i]=product%2;
			product/=2;
			sum+=tab[i];
			i++;
		}
		return sum;
}
	else
		cout <<"Podaj liczby wieksze lub rowne zero i mniejsze lub rowne 100 000 000"<<endl;
	return -1;
}

int main() {
	cout <<solution (3, 7)<<endl;
	cout <<solution (0, 0)<<endl;
	cout <<solution (0, 1)<<endl;
	cout <<solution (0, 0)<<endl;
	cout <<solution (100000000, 1)<<endl;
	cout <<solution (100000000, 100000000)<<endl;
	cout <<solution (-3, 7)<<endl;
	return 0;
}
