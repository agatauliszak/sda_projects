//============================================================================
// Name        : a.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

void f(){
	static int s=0;	//zastosowanie: zliczanie wywolan funkcji; static const int s=0 - stala w funkcji
	int a=0;
	cout<<"a wynosi: "<<++a<<"\t";
	cout<<"Funkcja wywolana "<<++s <<" razy"<<endl;
}

int main() {
	static int s=4;
	for (int i=0; i<10; ++i){
		f();
		s+=5;
		cout <<s<<endl;
	}

	return 0;
}
