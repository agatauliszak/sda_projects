//============================================================================
// Name        : funkcja_symbol_newtona.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int newton (int n, int k) {
	//cout << "Licze symbol newtona dla "<< n << " po "<< k<< endl;
	if (k==0 || k==n )
		return 1;
	else {
	int c = newton (n-1, k-1) + newton (n-1, k);
	//cout << "symbol ten dla " << n << " po "<< k << "wynosi " << endl;
			return c;
	}
}

int main() {
	int n, k=0;
		cout << "Podaj liczbe n i k dla symbolu newtona"<< endl;
		cin >>n>>k;
		cout <<"Symbol Newtona "<< n<< " po " <<k<< " wynosi " << newton (n,k)<< endl;
	return 0;
}
