//============================================================================
// Name        : funkcja_ciag_fibbonaciego.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int fib (int n) {
	//cout <<"licze ciag  fibb dla  " <<n<<endl;
	if (n==0)
		return 0;
	if (n==1)
		return 1;
	if (n>=2) {
		int c = fib (n-1) + fib (n-2);
	//cout << "ciag z " << n<< " wynosi " << c << endl;
		return c;
	}
}

int main() {
	int a=0;
	cout << "Podaj liczbe "<< endl;
	cin >>a;
	cout <<"Ciag Fibbonaciego dla liczby "<< a<< " wynosi " <<fib(a)<< endl;
	return 0;
}
