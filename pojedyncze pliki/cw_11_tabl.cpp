//============================================================================
// Name        : cw_11_tabl.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

// Unikalne wartosci w tablicy
#include <iostream>
using namespace std;

int main() {
	int t[10] = { 43, 5, 10, 7, 1, 43, 18, 5, 10, 43 };
	int licznik = 0; //liczba powtorzen
	int i, j = 0;

	for (i = 0; i < 10; i++) {
		for (j = i+1; j < 10; j++) {
			if (t[i] == t[j]) {
				licznik = licznik + 1;
				break;
			}
		}
	}
	cout << licznik<< endl;

	cout << "Liczba unikalnych wartosci  wynosi " << 10 - licznik << endl;
	return 0;
}
