//============================================================================
// Name        : deque.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <deque>
using namespace std;

int main() {
	deque <int> mydeque (2, 50);
	mydeque.push_front(100);
	mydeque.push_front(200);
	mydeque.push_back(300);
	mydeque.push_back(400);
	for (deque<int> :: iterator it =mydeque.begin(); it!=mydeque.end(); it++)
		cout<<*it<<' ';
	cout<<endl;
	return 0;
}
