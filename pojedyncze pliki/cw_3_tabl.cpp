//============================================================================
// Name        : cw_3_tabl.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
//Program znajdujący najmniejszą i największą wartość z tablicy liczb

#include <iostream>
using namespace std;

int main() {
	int t_min=20000;
	int t_max=-20000;

	int t[5]= {-5,1,3,7,10};

	for (int i=0; i<5; i++)
	{
		if (t[i]<t_min)
		{
			t_min=t[i];
		}

		if (t[i]>t_max)
		{
			t_max = t[i];
		}
	}

	cout <<"Max wynosi: " <<t_max << " a min: "<< t_min << endl;

	return 0;
}
