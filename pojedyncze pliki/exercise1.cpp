//============================================================================
// Name        : exercise1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int solution (int *A, int size_of_A){
	if (size_of_A>=1 && size_of_A<100000){
	int min=2147483647;

	for (int i=0; i<size_of_A; i++) {
		if (A [i]<min)
			min = A[i];
		}
	return min;
	}
	else
		cout<<"Nieprawidlowy rozmiar tablicy"<<endl;
	return 0;
}

int main() {
int tab [5]= {2, 4, 6, -10, 4};
cout<<solution (tab, 5)<<endl;
cout<<solution (tab, 0)<<endl;
int tab1 [5]= {2147483647, -2147483647, 6, -10, 4};
cout<<solution (tab1, 3)<<endl;
	return 0;
}
