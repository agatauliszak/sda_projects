//============================================================================
// Name        : maj_05.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

void Foo(){
	cout <<"global foo"<<endl;
}

void sizeTypes (){
	cout <<"size of bool: "<<sizeof(bool)<<endl;
	cout <<"size of char: "<<sizeof(char)<<endl;
	cout <<"size of int: "<<sizeof(int)<<endl;
	cout <<"size of long int: "<<sizeof(long int)<<endl;
	cout <<"size of double: "<<sizeof(double)<<endl;

}

class MyClass
{
public:
	enum types1 {
		TYPE_ONE,TYPE_TWO, ATYPE
	};
	enum type2 {
		AATYPE, BBTYPE
	};
	class Inner {
	public:
		void Foo(){
			::Foo ();
			Foo2();
			cout <<"Inner foo"<<endl;
		}

	};
	Inner inner;
	static int counter;
	static void Foo2() {
		static int counter_foo2;
		cout<<"Foo2 zostalo wywolane: "<<counter_foo2<<" razy"<<endl;
		counter_foo2++;
	}
	void Foo()
	{
		Foo2();
		counter ++;
	cout<<"Foo"<<endl;
	}

	const void Foo() const
	{
		Foo2();
		cout<<"Foo const"<<counter<<endl;
	}
};
int MyClass::counter=5;

int main() {
//	int n1=0, n2=3;
//	float n3=3, n4=6;
//	n1=(n3=(n2=5)*4/8.0)*2;
//	cout<<n1<<endl<<n2<<endl<<n3<<endl;
	MyClass cc;
	//const MyClass ccc;
	//const MyClass& ccc=cc;
	cc.Foo();
	cc.inner.Foo();
//	ccc.Foo();
	MyClass::Foo2();
	sizeTypes();
	int x=5;
	int y=6;
	int *p;
	p=&x;
	p=&y;
	*p=10;
	int &a=x;


	cout <<"x= "<<x<<endl;
	cout <<"a= "<<a<<endl;
	cout <<"&a= "<<&a<<endl;
cout <<"y= "<<y<<endl;
cout <<"p= "<<p<<endl;
cout <<"*p= "<<*p<<endl;


	return 0;
}
