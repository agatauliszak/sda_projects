//============================================================================
// Name        : funkcja_min.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int minimum (int a, int b) {
	int min=0;
	if (a>b) {
		min=b;
	}
	else
		min=a;
	return min;
}

int main() {
	int c, d;

	cout << "Podaj dwie liczby: "<< endl;
	cin >>c>>d;

	cout << "Mniejsza liczba to "<< minimum (c,d)<< endl;
	return 0;
}
