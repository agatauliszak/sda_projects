//============================================================================
// Name        : insertion_sort.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <ctime>
#include <iomanip>
#include <cstdlib>
using namespace std;

void insertion_sort (int size, int *A){
	for (int j=1; j<size; j++){
			int var = A [j];
			int i=j-1;
				while (i>=0 && A[i]>var) {
					A[i+1]=A[i];
					i--;
				}
			A[i+1]=var;
		}
}


int main() {
	int size, *A;
	cout<<"Podaj wielkosc zbioru: "<<endl;
	cin>>size;
	A= new int[size];

	srand(time(0));
	 for (int i=0; i<size; i++)
	 {
		 A[i]= rand ()%25;
	 }

	 cout <<"Przed sortowaniem: "<< endl;
	 	for (int k=0; k<size; k++){
	 		cout <<"Elemnet "<<k<<" tablicy: " <<A[k]<<endl;
	 	}
clock_t tStart = clock();
	 	insertion_sort (size, A);
cout <<"Time taken: "<<fixed<<setprecision(3)<<(1.0*clock() - tStart) / CLOCKS_PER_SEC<<"\n";

	cout <<"Po sortowaniu: "<< endl;
	for (int k=0; k<size; k++){
		cout <<"Element "<<k<<" tablicy: " <<A[k]<<endl;
	}
	return 0;
}
