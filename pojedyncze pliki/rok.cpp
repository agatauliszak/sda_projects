//============================================================================
// Name        : rok.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	int a;

	cout << "Podaj rok: " << endl;
	cin >> a;

	while (a < 0) {
		cout << "Wartosc bledna. Podaj wartosc wieksza od 0";
		cin >> a;
	}

	if ((a%4==0 && a%100!=0) || a%400==0)
	{
		cout << "Podany rok jest przestepny! " << endl;
	}
	else
	{
	cout << "Podany rok jest nieprzestepny! "<< endl;
	}

	return 0;
}
