//============================================================================
// Name        : cw_2_tabl.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
//Program wczytuj�cy podan� ilo�� liczb i wypisuj�cy je na ekran w odwrotnej kolejno�ci
#include <iostream>
using namespace std;
const int max_array_size = 5;

int main() {

	cout << "Program wpisujacy liczby i je zwracaj�ce. Podaj 5 liczb: " << endl;
	int t [max_array_size];

	for (int i = 0; i<max_array_size; i++)
	{
		cin >> t[i];
	}

	for (int i = max_array_size-1; i>=0; i--)
		{
			cout << t[i] << " ";
		}
	return 0;
}
