//============================================================================
// Name        : figury.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Figure.h"
#include "Circle.h"
#include "Triangle.h"
#include "Retangle.h"
#include "Square.h"
using namespace std;

int main() {
	Circle kolo (10);
	Triangle trojkat (10,5);
	Retangle prostokat (4,3);
	Square kwadrat (5);
	kwadrat.setSide(5);
	kwadrat.getName();
	cout<<kolo.getName()<<endl;
	cout<<trojkat.getName()<<endl;
	kolo.setRadius(1);
	cout<<kolo.getRadius()<<endl;
	cout<<kolo.area()<<endl;
	cout<<prostokat.getName()<<endl;
	cout <<trojkat.area()<<endl;
	cout<<prostokat.area()<<endl;
	cout<<kwadrat.area()<<endl;
	kolo.opis();
	trojkat.opis();
	//prostokat.drawTetra();
	kwadrat.drawTetra();
	trojkat.drawTriangle();
	//kolo.drawCircle();
	return 0;
}
