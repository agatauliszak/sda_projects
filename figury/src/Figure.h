/*
 * Figure.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef FIGURE_H_
#define FIGURE_H_

#include<string>
using namespace std;
#include <iostream>

class Figure {
public:
	string getName()
	{
		return string("Figura ta zwie si� ") + name;
	}

	virtual double area()=0;
protected:
	string name="zwyk�a figura";
};

#endif /* FIGURE_H_ */
