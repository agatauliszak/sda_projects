/*
 * Circle.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "Circle.h"
#include <cmath>


Circle::Circle(double radius) :
		r(radius) {
	name = "kolo";
}

double Circle::area() {
	double wynik;
	wynik = M_PI * pow(r, 2);
	return wynik;
}

void Circle::setRadius(double radius) {
	r = radius;
}

double Circle::getRadius() {
	return r;
}

void Circle::opis(){
	cout<< "Figura o nazwie "<<name<< " posiada promien o wartosci "<< getRadius()<<" cm"<<endl;
}

//void Circle::drawCircle()
//{
//
//for (int i=0; i<r; i++){
//	for ( int j=0; j<r; j++){
//		cout <<"* ";}
//	cout <<endl;
//}
//cout <<endl;
//}

