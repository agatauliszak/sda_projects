/*
 * Triangle.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_

#include "Figure.h"
#include<string>
using namespace std;
#include <iostream>

class Triangle: public Figure {
public:
	Triangle (int side, int height);
	void opis();
	double area();
	void drawTriangle();
private:
	int a;
	int h;
};
#endif /* TRIANGLE_H_ */
