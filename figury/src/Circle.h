/*
 * Circle.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_

#include "Figure.h"
#include<string>
#include<iostream>
using namespace std;

class Circle: public Figure {
public:
	Circle(double radius);

	void setRadius (double radius);
	double getRadius();
	double area();
	void opis();
	void drawCircle();

private:
	double r;
};

#endif /* CIRCLE_H_ */
