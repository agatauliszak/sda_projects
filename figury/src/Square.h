/*
 * Square.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef SQUARE_H_
#define SQUARE_H_

#include "Figure.h"
#include<string>
using namespace std;
#include <iostream>

class Square: public Figure {
public:
	Square(int side);
	Square();
	void setSide (int side);
	double area();
	void opis();
	void drawTetra();

protected:
	int a=2;
	int b=3;
};

#endif /* SQUARE_H_ */
