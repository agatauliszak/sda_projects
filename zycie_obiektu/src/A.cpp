/*
 * A.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "A.h"
#include <iostream>

A::A() {
	std::cout<<"Constructing A"<< std::endl;

}

A::~A() {
	std::cout<<"Destroying  A"<< std::endl;
}

std::ostream& operator <<(std::ostream& stream, const A& a) {
	stream <<"Content of A:\n\t number: "<<a.number <<"\n";
}
