/*
 * A.h
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef A_H_
#define A_H_

#include <iostream>

class A {
public:
	A();
	virtual ~A();
	int number;
};
std::ostream& operator <<(std::ostream& stream, const A& a);
#endif /* A_H_ */
