#include <iostream>

class Date {
public:
	Date();
	Date(int nday, int nmonth, int nyear);
	int getDay();
	int getMonth();
	int getYear();
	void setDay (int day);
	void setMonth (int month);
	void setYear (int year);
	bool isValid()const;
	void printDate()const;

	void addDay();
	void addMonth();
	void addYear();
	void addDays(int days);
	void addMonths(int months);
	void addYears(int years);
	int daysTillTheEndOfTheMonth()const;
	int daysTillTheEndOfTheYear()const;
	bool isInLappYear()const;
	bool operator==(const Date& other)const;
	bool operator!=(const Date& other)const;
	bool operator<(const Date& other)const;


private:
	int day;
	int month;
	int year;
};

