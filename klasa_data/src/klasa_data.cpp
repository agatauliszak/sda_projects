//============================================================================
// Namonthe        : klasa_data.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "data.h"

using namespace std;

int main() {
//	d.setDay(4);
//	d.setMonth(12);
//	d.setYear(2012);
	Date d(6, 12, 2015);
	Date a(5, 12, 2015);
//	Date h;
//	h=d;
	cout << (d == a) << endl;
	cout << (d != a) << endl;
	cout << (d < a) << endl;
//	cout << d.getDay()<<endl;
//	cout<<d.getMonth()<<endl;
//	cout<<d.getYear()<<endl;
//	d.printDate();
//	d.addDay();
//	cout << d.getDay()<<endl;
//	d.printDate();
//	cout <<d.isInLappYear()<<endl;
//	cout <<d.isValid();
	return 0;
}

int Date::getDay() {
	return day;
}

int Date::getMonth() {
	return month;
}

int Date::getYear() {
	return year;
}

void Date::setDay(int nday) {
	day = nday;
}

void Date::setMonth(int nmonth) {
	month = nmonth;
}
void Date::setYear(int nyear) {
	year = nyear;
}
bool Date::isValid() const {
	if (year > 0 && month > 0 && day > 0) {
		if (isInLappYear()) {
			if (((month == 1 || month == 3 || month == 5 || month == 7
					|| month == 9 || month == 11) && day <= 31)
					|| ((month == 4 || month == 6 || month == 8 || month == 10
							|| month == 12) && day <= 30)
					|| (month == 2 && day <= 29)) {
				cout << "Data poprawna " << endl;
				return true;
			} else
				cout << "Data niepoprawna!" << endl;
			return false;
		} else if (((month == 1 || month == 3 || month == 5 || month == 7
				|| month == 9 || month == 11) && day <= 31)
				|| ((month == 4 || month == 6 || month == 8 || month == 10
						|| month == 12) && day <= 30)
				|| (month == 2 && day <= 28)) {
			cout << "Data poprawna " << endl;
			return true;
		} else {
			cout << "Data niepoprawna!" << endl;
			return false;
		}
	} else
		cout << "Data niepoprawna!";
	return false;
}

void Date::printDate() const {
	cout << year << "-";
	if (month < 10) {
		cout << 0;	//dopisanie 0
	}
	cout << month << "-";
	if (day < 10) {
		cout << 0;
	}
	cout << day << endl;
}
void Date::addDay() {
	day = day + 1;
}

void Date::addMonth() {
	month = month + 1;
}

void Date::addYear() {
	year = year + 1;
}
bool Date::isInLappYear() const {
	if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
		return true;
	return false;
}

Date::Date(int nday, int nmonth, int nyear) {
	day = nday;
	month = nmonth;
	year = nyear;
}

Date::Date() {
	day = 1;
	month = 1;
	year = 1;
}

bool Date::operator ==(const Date& other) const {
	if (day == other.day && month == other.month && year == other.year)
		return true;
	return false;
}

bool Date::operator!=(const Date& other) const {
	return !operator==(other);
	return !(*this == other);
}

bool Date::operator<(const Date& other) const {
	if (year < other.year) {
		return true;
	} else if (year == other.year) {
		if (month < other.month) {
			return true;
		} else if (month == other.month && day < other.day) {
			return true;
		}
	}

	return false;
}

//int daysTillTheEndOfTheMonth()const{
//	if
//}
