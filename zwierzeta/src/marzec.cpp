//============================================================================
// Name        : marzec.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Animal.h"
#include "Dog.h"
#include "Cat.h"
#include "Insect.h"
#include "Coleoptera.h"
using namespace std;

int main() {
	//Animal ("anAnimal");
	Animal *someAnimal;
	Dog reksio("Reksio");
	Dog lajka("Lajka");
	Cat pankracy("Pankracy");
	Cat filemon("Filemon");
	Insect pszczola;
	filemon.speak();
//	anAnimal.speak();
	reksio.speak();
	pankracy.speak();
	//Animal &someAnimal = reksio;
	//someAnimal.speak();
	//someAnimal=&reksio;
//	someAnimal->speak();//do psa odnosimy sie wskaznikiem dla zwirzecia, wiec nie szczeka
//	someAnimal = &pankracy;
//	someAnimal->speak();
const int numberOfAnimals=4;
Animal* animals[numberOfAnimals];
cout <<"\t wypisanie tablicy: "<< endl;

	animals[0] =&reksio;
	animals[1]=&pankracy;
	animals[2]=&lajka;
	animals[3]=&filemon;
for (int i=0; i<4; i++) {
	animals[i]->speak(); //dostep do elementow klasy przez wskaznik za pomoca operatora ->
}

std::cout<<endl;
std::cout<<reksio.giveName()<<std::endl;
std::cout<<pankracy.giveName()<<std::endl;
reksio.ileNog(4);
Coleoptera rosalia;
rosalia.ileNog();
rosalia.giveASound();
std::cout<<rosalia.giveName()<<std::endl;
pszczola.ileNog();
pszczola.giveASound();
std::cout<<pszczola.giveName()<<std::endl;

	return 0;
}
