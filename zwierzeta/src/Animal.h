/*
 * Animal.h
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef ANIMAL_H_
#define ANIMAL_H_

#include<string>


class Animal {
public:
	Animal(std::string givenName); //konstruktor
	virtual void speak(); // f wirtualna w klasach pochodnych jest implementowana adekwatnie do klasy
	std::string giveName()
	{
		return std::string("my name is: ") + name;
	}

void ileNog(int nogi);

private:
	virtual void giveASound()=0;//metoda czysto wirtualna a wiec klasa jest abstrakcyjna, nie mozemy utworzyc egzemplarza
	std::string name;
};

#endif /* ANIMAL_H_ */
