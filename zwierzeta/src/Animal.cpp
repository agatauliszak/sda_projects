/*
 * Animal.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include "Animal.h"
#include <iostream>

Animal::Animal(std::string givenName) :
name(givenName) {
} //konstruktor

void Animal::speak()
{
	std::cout <<name<<" speaks: ";
	giveASound();
	//std::cout<< std::endl;
}

void Animal::ileNog(int nogi)
{
	std::cout <<"to zwierze ma " <<nogi<< " nogi"<< std::endl;
}
