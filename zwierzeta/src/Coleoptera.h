/*
 * Coleoptera.h
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef COLEOPTERA_H_
#define COLEOPTERA_H_

#include "Insect.h"

class Coleoptera: public Insect {
public:

	void giveASound();
};

#endif /* COLEOPTERA_H_ */
