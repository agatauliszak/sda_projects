/*
 * Dog.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include "Dog.h"
#include <iostream>

Dog::Dog(std::string givenName) :
		Animal(givenName) {
}

int b = 4;

void Dog::giveASound() {
	std::cout << "Hau hau!" << std::endl;
}

void Dog::ileNog(int nogi) {
	std::cout << "pies ma " << nogi << " nogi" << std::endl;
}
