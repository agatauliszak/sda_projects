/*
 * Insect.h
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef INSECT_H_
#define INSECT_H_

#include "Animal.h"

class Insect: public Animal {
public:
	Insect();
	void ileNog();
	void giveASound();
	int nogi= 6;

};

#endif /* INSECT_H_ */
