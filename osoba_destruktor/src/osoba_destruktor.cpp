//============================================================================
// Name        : osoba_destruktor.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Osoba.h"
using namespace std;

int main() {
	Osoba pierwsza;
	pierwsza.setName("Adam");
	pierwsza.setSurname("Kadmon");
	pierwsza.setAge(120);
	Osoba druga;
	druga.setName("Lilith");
	druga.setSurname("Babilonia");
	druga.setAge(111);
	pierwsza.createMail();
	pierwsza.description();
//	druga.description();
//	Osoba *tab_osoba[3]={&pierwsza, &druga};
//	tab_osoba[0]->description();
//	Osoba *wsk;
//	wsk=&pierwsza;
//	wsk->description();
	pierwsza.changeAge(pierwsza);
	pierwsza.description();
	return 0;
}
