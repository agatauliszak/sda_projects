/*
 * Osoba.h
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef OSOBA_H_
#define OSOBA_H_
#include <string>
#include <iostream>

using namespace std;

class Osoba {
public:
	Osoba();
	virtual ~Osoba();
	void description();
	string createMail();
	void setName(string name);
	void setSurname(string surname);
	void setAge(int age);
	Osoba changeAge (Osoba ktos);

private:
string Imie;
string Nazwisko;
int wiek;

};

#endif /* OSOBA_H_ */
