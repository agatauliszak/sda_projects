/*
 * Osoba.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "Osoba.h"

Osoba::Osoba() {
	std::cout<<"Constructing object Osoba"<< std::endl;

}

Osoba::~Osoba() {
	std::cout<<"Destroying  object Osoba"<< std::endl;
}

void Osoba::description()
{
	cout << "Nazywam sie "<<Imie<< " " << Nazwisko<< ", mam "<<wiek<< " lat. Moj mail: "<<createMail()<< endl;
}

string Osoba::createMail()
{
	return  Imie + "."+Nazwisko+("(malpa).paradise.ea");
}

void Osoba::setName(string name){
	 Imie=name;
}
	void Osoba::setSurname(string surname){
		Nazwisko=surname;
	}
	void Osoba::setAge(int age){
		wiek=age;
	}

	Osoba Osoba::changeAge (Osoba ktos){
		cout<<"Wiek osoby przed funkcja: "<<ktos.wiek<<endl;
		ktos.wiek+=5;
		cout<<"Wiek osoby po wykonaniu funkcji: " <<ktos.wiek<<endl;
		return ktos;
}
