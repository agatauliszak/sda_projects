//============================================================================
// Name        : casting.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
#include "Event.h"
#include "OpenFileEvent.h"

void displayEvent (Event *event)
{
	cout<<"received: "<<event->getEventType()<<"\n";
	if(event->getEventType()=="OpenFileEvent")
	{
		OpenFileEvent *openEvent = static_cast<OpenFileEvent*>(event);
		cout<<"Trying to open file: "<<openEvent->getFileName();
	}
//	OpenFileEvent *openEvent = dynamic_cast <OpenFileEvent*>(event);
//	if (openEvent)
//	{
//		cout<<"Got an openEvent!";
//	}
	cout<<"\n";
}

int main() {
//	float  aNumber = 3.5;
//	int someOtherNumber = static_cast<int>(aNumber);
//	int someOtherNumberB = (int)aNumber;
//	int someOtherNumberC = int(aNumber);

	Event gev = Event();
	OpenFileEvent oev ("a.txt");
	Event *ev= &gev;
	cout <<ev->getEventType()<<"\n";

	displayEvent(ev);
	ev= &oev;
	cout <<ev->getEventType()<< "\n";
	displayEvent(ev);
	return 0;
}
