/*
 * GameObject.h
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef GAMEOBJECT_H_
#define GAMEOBJECT_H_

class GameObject {
public:
	GameObject();
	virtual ~GameObject();
	virtual char getSymbol();

	int getX() const {
		return x;
	}

	int getY() const {
		return y;
	}

	void setX(int _x) {
		x = _x;
	}

	void setY(int _y) {
		y = _y;
	}

private:
	int x,y;
};

#endif /* GAMEOBJECT_H_ */
