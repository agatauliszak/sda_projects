/*
 * KeyManager.h
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef KEYMANAGER_H_
#define KEYMANAGER_H_

class KeyManager {
public:
	KeyManager();
	virtual ~KeyManager();
	void addNewKey(char _key);
	char getNextKey ();
private:
	char key;
};

#endif /* KEYMANAGER_H_ */
