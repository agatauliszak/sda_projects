/*
 * GameLogic.h
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef GAMELOGIC_H_
#define GAMELOGIC_H_
#include "Player.h"
#include "Enemy.h"
#include "GameObject.h"
#include "KeyManager.h"

class GameLogic {
public:
	GameLogic();
	virtual ~GameLogic();
	void configureGame();
	bool nextTurn();//zwraca informacje na temat konca gry
	void randomPosition (GameObject* object);
	bool checkPosition (int x, int y);
	bool checkPlayerPosition();

	 GameObject** getObjectList()  {
		return object_list;
	}

	void setKeyManager(KeyManager*& keyManager) {
		key_manager = keyManager;
	}

private:
	void updatePlayerPosition();
	KeyManager *key_manager;
	Player *player;
	Enemy *enemies[5];
	GameObject *object_list[6]; //wspisany tu jest player i wszyscy enemy

};

#endif /* GAMELOGIC_H_ */
