//============================================================================
// Name        : game.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "KeyManager.h"
#include "Map.h"
#include "MapDisplayer.h"
#include "GameLogic.h"

using namespace std;

void printHelp(){
	cout<<"Help: "<<endl<<"o - exit"<<endl;
	cout<<"r-idle"<<endl<<"w-up"<<endl<<"s-down"<<endl<<"a-left"<<endl<<"d-right"<<endl;
}

int main() {
	bool exit=false;
	KeyManager *key_manager=new KeyManager; //utworzenie obiektu KeyMenager
	Map *map=new Map; //utworzenie obiektu mapy
	GameLogic *game_logic=new GameLogic;//utworzenie obiektu Logiki
	MapDisplayer *map_displayer=new MapDisplayer; //utworzenie obiektu wyswietlajacego mape
	map_displayer->setMap(map); //przekazanie mapy do map displayera
	map->setObjectList(game_logic->getObjectList());
	game_logic->setKeyManager (key_manager);

	game_logic->configureGame();
	map->generateMap();
	map_displayer->displayMap();

	while (exit==false){	//petla glowna programu
		//sprawdzic czy gra jest skonfigurowana
		char key;
		cout <<"Please enter key and press enter.(h for help)"<<endl;//popros gracza o kolejny znak
		cin>>key;
		if(key=='h'){
			printHelp();
			continue;
		}
		key_manager->addNewKey(key); 	//wczytanie kolejnego znaku

		if (game_logic->nextTurn()){//Logika gry
			map->generateMap();
			map_displayer->displayMap();
			exit=true;
			cout <<"Gotcha! GAME OVER!"<<endl;
			continue;
		}
		map->generateMap();//generowanie mapy
		map_displayer->displayMap();//wyswietlanie mapy
	}
	delete key_manager;//usuniecie obiektu KeyMenager
	delete game_logic;//usuniecie obiektu Logiki
	delete map;//usuniecie obiektu mapy
	delete map_displayer;//usuniecie obiektu wyswietlajacego mape
	cout<<"Game ended successfully"<<endl;
	return 0;
}
