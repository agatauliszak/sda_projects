/*
 * GameLogic.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "GameLogic.h"
#include <stdlib.h>
#include <time.h>
#include <math.h>

GameLogic::GameLogic() {
	// TODO Auto-generated constructor stub
	player=nullptr;
	for (int i=0; i<5; i++){
		enemies [i]=nullptr;
	}
	srand(time(NULL));
}

GameLogic::~GameLogic() {
	// TODO Auto-generated destructor stub
	if (player !=nullptr) {
		delete player;
	}

	for (int i=0; i<5; i++) {
		if (enemies[i] != nullptr) {
			delete enemies[i];
		}
	}
}

void GameLogic::configureGame() {
	if (player !=nullptr) {
		delete player;
	}
	for (int i=0; i<5; i++) {
		if (enemies[i] != nullptr) {
			delete enemies[i];
		}
	}

	player=new Player;
	object_list [0]=player;
	for (int i=0; i<5; i++){
		enemies[i]=new Enemy;
		object_list[i+1]=enemies[i];
	}
	for (int i=0; i<6; i++){
		randomPosition(object_list[i]);
	}
	while (checkPlayerPosition()==false){
		randomPosition(player);
	}
}

bool GameLogic::nextTurn() {
	if (key_manager->getNextKey()=='o'){
		return true;
	}
	updatePlayerPosition();//wygenerowania pozycji gracza i przeciwnikow
	//wygenerowanie nowej pozycji gracza
	//wygenerowanie nowej pozycji przeciwnikow
	if (checkPlayerPosition()==false){
		return true;
	}
	//sprawdzanie czy przeciwnik jest w odleglsoci pola od gracza
	return false;
}

void GameLogic::randomPosition(GameObject* object) {
	int x,y;
	bool success=false;

	while (success==false){
	x=rand()%25; //generujemy pozycje
	y=rand()%25;
	if (checkPosition (x,y)){//sprawdzimy pozycje
		object->setX(x);
		object->setY(y);
		success=true;
		}
	}
}

bool GameLogic::checkPosition(int x, int y) {
	for (int i=0; i<6; i++){
		if (object_list[i]->getX()==x && object_list[i]->getY()==y){
			return false;
		}
	}
	return true; //pozycja jest pusta
}

bool GameLogic::checkPlayerPosition() {//jak true pozycja jest ok, false oznacza przegrana
	for (int i=1; i<6; i++){
		if (abs(object_list[i]->getX()-player->getX()) <=1 && abs(object_list[i]->getY()-player->getY()) <=1){
			return false;
		}
	}
	return true;
}

void GameLogic::updatePlayerPosition() {
	switch(key_manager->getNextKey()){
	case 'w':
		if (player->getY()-1>=0){
			player->setY(player->getY()-1);
		}
		break;
	case 's':
		if (player->getY()+1<240){
			player->setY(player->getY()+1);
		}
		break;
	case 'a':
		if (player->getX()-1>=0){
			player->setX(player->getX()-1);
		}
		break;
	case 'd':
		if (player->getX()+1<24){
			player->setX(player->getY()+1);
		}
		break;
	}
}
