/*
 * Map.h
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef MAP_H_
#define MAP_H_
#include "GameObject.h"

class Map {
public:
	Map();
	virtual ~Map();
	void generateMap();
	char getPoint(int x, int y);

	void setObjectList( GameObject** objectList) {
		object_list = objectList;
	}

private:
	char map[25][25];
	GameObject **object_list;
};

#endif /* MAP_H_ */
