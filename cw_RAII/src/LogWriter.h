/*
 * LogWriter.h
 *
 *  Created on: 10.04.2017
 *      Author: RENT
 */

#ifndef LOGWRITER_H_
#define LOGWRITER_H_
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class LogWriter {
public:
	LogWriter ();
	virtual ~LogWriter();

	enum LogType {Fatal, Error, Warning, Info, Debug, Trace};

	void logMessage (string message, LogType messageType);
	void setLogginLevel(LogType nlogLevel);
	string convertType (LogType messageType);

private:
	fstream f;
	LogType logLevel;
};

#endif /* LOGWRITER_H_ */
