//============================================================================
// Name        : cw_RAII.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include "LogWriter.h"
using namespace std;

int main() {
	LogWriter g;
	g.logMessage("puk puk", LogWriter::Debug);
	g.logMessage("sprawdzam dla fatal",LogWriter::Fatal);
	g.logMessage("sprawdzam dla info",LogWriter::Info);
	return 0;
}
