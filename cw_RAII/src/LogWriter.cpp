/*
 * LogWriter.cpp
 *
 *  Created on: 10.04.2017
 *      Author: RENT
 */

#include "LogWriter.h"

LogWriter::LogWriter(): logLevel(LogWriter::Info) {
	f.open("login.log",ios_base::out);
}

LogWriter::~LogWriter() {
	f.close();
}

void LogWriter::setLogginLevel(LogType nlogLevel) {
	logLevel = nlogLevel;
};

void LogWriter:: logMessage (string message, LogType messageType)
{ if (messageType<=logLevel) {
	f<<message<<"\t";
	f<<convertType(messageType)<<endl;

}
}

string LogWriter::convertType(LogType messageType) {
	switch (messageType)
	{
	case 0:
		return "wiadomosc jest typu fatal";
	case 1:
		return "wiadomosc jest typu error";
	case 2:
		return "wiadomosc jest typu warning";
	case 3:
			return "wiadomosc jest typu info";
	}
}
