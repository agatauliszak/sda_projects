/*
 * Singleton.cpp
 *
 *  Created on: 10.05.2017
 *      Author: RENT
 */

#include "Singleton.h"
#include <iostream>

Singleton* Singleton::instance=0;

Singleton::Singleton() {
}

Singleton::~Singleton() {
	// TODO Auto-generated destructor stub
}

  Singleton& Singleton::getInstance(){
	static Singleton* instance =new Singleton;
	return *instance;
}
