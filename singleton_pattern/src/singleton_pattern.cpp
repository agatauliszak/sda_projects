//============================================================================
// Name        : singleton_pattern.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include "Singleton.h"
#include <iostream>
using namespace std;

int main() {
	Singleton &a=Singleton::getInstance();
	cout<<&(a.getInstance())<<endl;
	cout<<&(a.getInstance());
	return 0;
}
