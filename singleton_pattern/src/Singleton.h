/*
 * Singleton.h
 *
 *  Created on: 10.05.2017
 *      Author: RENT
 */

#ifndef SINGLETON_H_
#define SINGLETON_H_

class Singleton {
public:
	int getAInt(){
		return 1;
	}
	virtual ~Singleton();
	static Singleton& getInstance();

private:
	static Singleton * instance;
	Singleton();
	Singleton  (const Singleton &);
};

#endif /* SINGLETON_H_ */
